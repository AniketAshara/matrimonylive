package com.example.matrimonylive.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.content.Intent;

import com.example.matrimonylive.R;


public class MainActivity extends AppCompatActivity {

    ImageView activity_main_registration,activity_main_favorite_user, activity_main_user_list, activity_main_favorite,activity_main_search, activity_favorite_user;
    Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView activity_main_registration = (ImageView) findViewById(R.id.activity_main_registration);

        activity_main_registration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, AddUserActivity.class));
            }
        });


        ImageView activity_main_user_list = (ImageView) findViewById(R.id.activity_main_user_list);

        activity_main_user_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, UserListActivity.class));
            }
        });

        ImageView activity_main_favorite = (ImageView) findViewById(R.id.activity_main_favorite);

        activity_main_favorite_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, FavoriteActivity.class));
            }
        });


        ImageView activity_main_search = (ImageView) findViewById(R.id.activity_main_search);

        activity_main_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, SearchUserActivity.class));
            }
        });



    }
}



